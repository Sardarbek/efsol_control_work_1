﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Еfsol_ControlWork_1.Repositories;
using Еfsol_ControlWork_1.Repositories.Contracts;

namespace Еfsol_ControlWork_1.Controllers
{
    public class TeamController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public TeamController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teams = unitOfWork.Teams.GetAll();
                return View(teams);
            }
        }
    }
}