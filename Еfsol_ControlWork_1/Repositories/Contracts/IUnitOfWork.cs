﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Еfsol_ControlWork_1.Models;
using Еfsol_ControlWork_1.Repositories.Contracts;

namespace Еfsol_ControlWork_1.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        ITeamRepository Teams { get; }
        IPlayerRepository Players { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        //void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
