﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Еfsol_ControlWork_1.Models;

namespace Еfsol_ControlWork_1.Repositories.Contracts
{
    public interface ITeamRepository : IRepository<Team>
    {
    }
}
