﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Еfsol_ControlWork_1.Models;

namespace Еfsol_ControlWork_1.Repositories.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        void Add(T entity);
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
