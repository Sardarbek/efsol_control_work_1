﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using Еfsol_ControlWork_1.Models;
using Еfsol_ControlWork_1.Repositories.Contracts;

namespace Еfsol_ControlWork_1.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositores;

        private IDbContextTransaction _transaction;

        private bool _disposed;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositores = new ConcurrentDictionary<Type, object>();

            Teams = new TeamRepository(context);
            Players = new PlayerRepository(context);
        }

        public ITeamRepository Teams { get; }

        public IPlayerRepository Players { get; }

        public Task<int> CompleteAsync()
        {
            return _context.SaveChangesAsync();
        }
        public void BeginTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        //public void BeginTransaction(IsolationLevel level)
        //{
        //    _transaction = _context.Database.BeginTransaction(level);
        //}

        public void RollbackTransaction()
        {
            if (_transaction == null) return;

            _transaction.Rollback();
            _transaction.Dispose();

            _transaction = null;
        }
        public void CommitTransaction()
        {
            if (_transaction == null) return;

            _transaction.Commit();
            _transaction.Dispose();

            _transaction = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if(disposing)
                _context.Dispose();

            _disposed = true;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return _repositores.GetOrAdd(typeof(TEntity),new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}
