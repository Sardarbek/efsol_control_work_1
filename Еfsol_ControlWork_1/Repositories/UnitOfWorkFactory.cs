﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Еfsol_ControlWork_1.Repositories.Contracts;

namespace Еfsol_ControlWork_1.Repositories
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public IUnitOfWork MakeUnitOfWork()     
        {
            return new UnitOfWork(_applicationDbContextFactory.Create());
        }
    }
}
