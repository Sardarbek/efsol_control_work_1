﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Еfsol_ControlWork_1.Models;
using Еfsol_ControlWork_1.Repositories.Contracts;

namespace Еfsol_ControlWork_1.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {

        public TeamRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Teams;
            
        }
    }
}
