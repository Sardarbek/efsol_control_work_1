﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Еfsol_ControlWork_1.Models;
using Еfsol_ControlWork_1.Repositories.Contracts;

namespace Еfsol_ControlWork_1.Repositories
{
    public class PlayerRepository : Repository<Player>, IPlayerRepository   
    {
        public PlayerRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Players;
        }
    }
}
