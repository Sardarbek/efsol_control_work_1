﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Еfsol_ControlWork_1.Models
{
    public class Player : Entity
    { 
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Age { get; set; }
        public string Position { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }
    }
}
